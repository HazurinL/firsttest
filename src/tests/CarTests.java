package tests;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {

	@Test
	void testGetSpeed() {
		Car car = new Car(10);
		assertEquals(10,car.getSpeed());
		}
	
	@Test
	void testException() throws IllegalArgumentException{
		assertThrows(IllegalArgumentException.class, () -> {
        	Car car = new Car(-1);
        });
	}
	
	@Test
	void testLocation() {
		Car car = new Car(1);
		car.moveRight();
		assertEquals(1,car.getLocation());
		Car car2 = new Car(3);
		car2.moveLeft();
		assertEquals(-3,car2.getLocation());
	}
	
	@Test
	void testAccelaration() {
		Car car = new Car(1);
		car.accelerate();
		assertEquals(2,car.getSpeed());
		}
	
	@Test
	void testDecelerate() {
		Car car = new Car(5);
		car.decelerate();
		assertEquals(4, car.getSpeed());
		Car car2 = new Car(0);
		car2.decelerate();
		assertEquals(0,car2.getSpeed());
	}
}
