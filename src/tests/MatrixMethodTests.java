package tests;
import utilities.MatrixMethod;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void test() {
		int[][] testArrIn = {{1,2,3},{4,5,6}};
		int[][] testArrExpected = {{1,2,3,1,2,3},{4,5,6,4,5,6}};
		int[][] testArrOut = MatrixMethod.duplicate(testArrIn);
		assertArrayEquals(testArrExpected, testArrOut);
		
	}
}
